package java8.collector.groupingBy;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class GroupingBySample {

    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Jane", 42),
                new Person("Bob", 63),
                new Person("Sara", 33),
                new Person("Bill", 33),
                new Person("Jill", 12),
                new Person("Jack", 2),
                new Person("Jack", 75));

        printLessonSeparator("Java 8 Collector: groupingBy()");

        Map<Integer, List<Person>> groupedByAge =
                people.stream()
                      .sorted(Comparator.comparing(Person::getAge))
                      .collect(groupingBy(Person::getAge));

        System.out.println("groupedByAge = " + groupedByAge);
        System.out.println();

        Map<String, List<Person>> groupedByName =
                people.stream()
                      .sorted(Comparator.comparing(Person::getAge))
                      .collect(groupingBy(Person::getName));

        System.out.println("groupedByName = " + groupedByName);
        System.out.println();

        Map<Integer, List<Person>> groupedByNameLength =
                people.stream()
                      .sorted(Comparator.comparing(Person::getAge))
                      .collect(groupingBy(person -> person.getName().length()));

        System.out.println("groupedByNameLength = " + groupedByNameLength);
        System.out.println();

        printLessonSeparator("Java 8 Collector: Classifer for groupingBy()");

        System.out.println("printGrouping byAge: ");
        System.out.println("Map<Integer, List<java8.collector.groupingBy.Person>>()");
        printGrouping(people.stream(), Person::getAge);

        System.out.println();
        System.out.println("printGrouping byName: ");
        System.out.println("Map<String, List<java8.collector.groupingBy.Person>>()");
        printGrouping(people.stream(), Person::getName);

        System.out.println();
        System.out.println("printGrouping byName.charAt(0): ");
        System.out.println("Map<Character, List<java8.collector.groupingBy.Person>>()");
        printGrouping(people.stream(), person -> person.getName().charAt(0));

        System.out.println();
        printLessonSeparator("Java 8 Collector: Mapping groupingBy");

        Map<String, List<Person>> grouped =
                people.stream()
                      .collect(groupingBy(Person::getName));

        // Hard way
        // Broken:
        //      1) Print age as value not stream reference
        //      2) Need to collect result into map

//        grouped.keySet()
//                .stream()
//                .map(key -> new AbstractMap.SimpleEntry<>(key,
//                        grouped.get(key).stream().map(java8.collector.groupingBy.Person::getAge)))
//                .forEach(System.out::println);

        // Easy Way
        Map<String, List<Integer>> groupedEasy =
                people.stream()
                      .collect(groupingBy(Person::getName,
                                          mapping(Person::getAge, toList())));

        System.out.println();
        System.out.println("groupedEasyWay: ");
        System.out.println("Map<String, List<Integer>>()");
        System.out.println(groupedEasy);
        System.out.println();

        printLessonSeparator("Java 8 Collector: Reducing groupBy-- Part I");

        // Given the list of people, create a map
        // with the key as the first character of each persons' name
        // and the value as the oldest person whose name begins with the first character key

        // Hard way
        Map<Character, List<Person>> groupReduceHard =
                people.stream()
                      .collect(groupingBy(person -> person.getName().charAt(0)));

        BinaryOperator<Person> eldest = (person1, person2) ->
                person1.getAge() > person2.getAge() ? person1 : person2;

        System.out.println("groupReduce hard: ");
        groupReduceHard.keySet()
                       .stream()
                       .map(key -> groupReduceHard.get(key)
                                                  .stream()
                                                  .reduce(eldest))
                       .forEach(person -> {
                           if (person.isPresent()) {
                               System.out.println(person.get());
                           }
                       });

        System.out.println();

        // Easy way
        Function<Person, Character> firstLetter = person -> person.getName().charAt(0);

        Comparator<Person> byAge = (person1, person2) ->
                person1.getAge() > person2.getAge() ? 1 : -1;


        // Using maxBy for this example, minBy is also available
        Map<Character, Optional<Person>> groupReduceEasy =
                people.stream()
                      .collect(groupingBy(firstLetter, maxBy(byAge)));

        System.out.println("groupReduce easy:");
        System.out.println(groupReduceEasy);
        System.out.println();

        // Example of finding person with longest name
        Comparator<Person> byLongestName = (person1, person2) ->
                person1.getName().length() > person2.getName().length() ? 1 : -1;

        Map<Character, Optional<Person>> groupReduceLongestName =
                people.stream()
                      .collect(groupingBy(firstLetter, maxBy(byLongestName)));

        System.out.println("groupReduce byLongestName:");
        System.out.println(groupReduceLongestName);
        System.out.println();

        printLessonSeparator("Java 8 Collector: Reducing groupBy-- Part II");

        // Same length for name, then select the elder from the two
        // Person with longer name comes out ahead

        BinaryOperator<Person> criteria = (person1, person2) -> {
            if (person1.getName().length() == person2.getName().length()) {
                return person1.getAge() > person2.getAge() ? person1 : person2;
            } else {
                return person1.getName().length() > person2.getName().length() ? person1 : person2;
            }
        };

        Map<Character, Optional<Person>> groupWithCriteria =
                people.stream()
                      .collect(groupingBy(firstLetter, reducing(criteria)));

        System.out.println("groupWithCriteria");
        System.out.println(groupWithCriteria);
    }

    private static void printLessonSeparator(String lessonName) {
        System.out.println("===========================================");
        System.out.println(lessonName);
        System.out.println();
    }

    public static <T> void printGrouping(Stream<Person> people, Function<Person, T> classifier) {
        System.out.println(people.collect(groupingBy(classifier)));
    }
}
