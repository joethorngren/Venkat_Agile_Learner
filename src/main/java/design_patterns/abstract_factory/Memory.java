package design_patterns.abstract_factory;

public abstract class Memory {

    @Override
    public String toString() {
        return String.format("%s %d", getClass(), hashCode());
    }

}
