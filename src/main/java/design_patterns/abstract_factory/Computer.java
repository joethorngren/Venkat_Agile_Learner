package design_patterns.abstract_factory;

import sun.plugin2.gluegen.runtime.CPU;

public abstract class Computer {

    private Cpu cpu;
    private Memory memory;

    public abstract String getPartsType();

    @Override
    public String toString() {
        return String.format("%s %s %s", getClass(), cpu, memory);
    }

    public void add(Cpu cpu) {
        this.cpu = cpu;
    }

    public void add(Memory memory) {
        this.memory = memory;
    }

}
