package design_patterns.abstract_factory;

public class ComputerFactoryA implements IComputerFactory {
    @Override
    public Cpu getCpu() {
        return new CpuA();
    }

    @Override
    public Memory getMemory() {
        return new MemoryA();
    }
}
