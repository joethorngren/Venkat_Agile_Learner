package design_patterns.abstract_factory;

public abstract class Cpu {

    @Override
    public String toString() {
        return String.format("%s %d", getClass(), hashCode());
    }

}
