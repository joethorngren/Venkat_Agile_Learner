package design_patterns.abstract_factory;

public interface IComputerFactory {
    public Cpu getCpu();

    public Memory getMemory();
}
