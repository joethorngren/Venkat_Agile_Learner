package design_patterns.abstract_factory;

public class ComputerFactoryB implements IComputerFactory {
    @Override
    public Cpu getCpu() {
        return new CpuB();
    }

    @Override
    public Memory getMemory() {
        return new MemoryB();
    }
}
