package design_patterns.abstract_factory;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        abstractFactoryCreateComputers();

        reflectionCreateComputers();
    }

    private static void abstractFactoryCreateComputers() {
        ComputerA computer1 = new ComputerA();
        createComputer(computer1, new ComputerFactoryA());
        System.out.println(computer1);

        ComputerB computer2 = new ComputerB();
        createComputer(computer2, new ComputerFactoryB());
        System.out.println(computer2);
    }

    private static void reflectionCreateComputers() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        ComputerA computer1 = new ComputerA();
        createComputer(computer1);
        System.out.println(computer1);

        ComputerB computer2 = new ComputerB();
        createComputer(computer2);
        System.out.println(computer2);
    }

    private static void createComputer(Computer computer) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        String partsType = computer.getPartsType();
        String packageName = "com.dayFun.designPatterns.abstractFactory.";

        computer.add((Cpu) Class.forName(packageName + "Cpu" + partsType).newInstance());
        computer.add((Memory) Class.forName(packageName + "Memory" + partsType).newInstance());
    }

    private static void createComputer(Computer computer, IComputerFactory factory) {
        computer.add(factory.getCpu());
        computer.add(factory.getMemory());
    }
}
