package design_patterns.abstract_factory;

public class ComputerB extends Computer {

    @Override
    public String getPartsType() {
        return  "B";
    }
}
