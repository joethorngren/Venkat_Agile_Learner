package design_patterns.abstract_factory;

public class MemoryB extends Memory {

    @Override
    public String toString() {
        return String.format("%s %d", getClass(), hashCode());
    }
}
