package design_patterns.abstract_factory;

public class ComputerA extends Computer {

    @Override
    public String getPartsType() {
        return "A";
    }
}
